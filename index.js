const tmi = require("tmi.js");
const options = require(`./options.${process.env.NODE_ENV || 'local'}.js`); //Your options file
const fetch = require('node-fetch');
const axios = require('axios');
// Connect to twitch server
const client = new tmi.client(options);
client.connect();
// Initial connection to channels that should be active
// because in the case pm2 restarts for any reason we 
// will need to rejoin the active channels
client.on("connected", (address, port) => {
  let targetChannels = ['gravshark'];

  // join the bot to the channels to receive chat messages
  targetChannels.forEach(function(element){
    client.join(element);
  });
  
});


// on chat message received
client.on("chat", (channel, userstate, message, self) => { 


    if(message.startsWith("!p ")){
      url = 'https://tarkov-market.com/api/v1/item?q=' + message.replace("!p ", "") + 'PASTE_API_KEY_HERE_IN_PROD';
      // run the fetch
      fetch(url)
      .then(jsonData => jsonData.json())
      .then(data => botResponse(data));



      let botResponse = (data) => {

        diff24h = data[0].diff24h;
        diff7d = data[0].diff7days;


        if(Math.sign(diff24h) == 1){
          diff24h = "+" + Math.round(diff24h);
        }
        else if(Math.sign(diff24h) == -1){
          diff24h = Math.round(diff24h);
        }


        if(Math.sign(diff7d) == 1){
          diff7d = "+" + Math.round(diff7d);
        }
        else if(Math.sign(diff7d) == -1){
          diff7d = Math.round(diff7d);
        }

        pricePerSlot = Math.round(data[0].price / data[0].slots)

        itemName = data[0].name
        itemNameShort = data[0].shortName

        if(itemName.length > 15){
          itemName = itemNameShort;
        }



        // build the response string
        responseString = itemName + ": " + data[0].price
        + "₽. 24hr/7d price changes are: "
        + diff24h + "%/"
        + diff7d + "%. "
        + data[0].traderName + " buys it for "
        + data[0].traderPrice + data[0].traderPriceCur
        + ". Price per slot (per Flea Market prices) is "
        + pricePerSlot + "₽.";


        client.say(channel, responseString);

      }
    }

  }

);
